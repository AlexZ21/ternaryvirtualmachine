#ifndef TRYTE_H
#define TRYTE_H

#include "trit.h"
#include <string>
#include <math.h>

class Tryte
{
    Trit *value;
    const int size = 9;

public:
    Tryte();
    ~Tryte();

    const Tryte& operator = (const Tryte &tr);
    const Tryte& operator = (const int &num);

    int operator <(const Tryte& tr);
    int operator >(const Tryte& tr);
    int operator ==(const Tryte& tr);
    int operator !=(const Tryte &tr);

    int sizeOf();

    Trit *getTritArray();
    void insertBackTrit(int state);

    Tryte &inverse();

    Tryte &fromDecimal(int number);
    int toDecimal();

    Tryte &fromString(const std::string &str);
    std::string toString();
};

#endif // TRYTE_H
