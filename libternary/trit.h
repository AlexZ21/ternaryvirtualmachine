#ifndef TRIT_H
#define TRIT_H

class Trit{
    static const char *additionTable[3][3];
    static const char multiplicationTable[3][3];
public:
    enum {
        T = -1, O = 0, I = 1
    };
    int state;

    static int getStateFromChar(const char &ch)
    {
        switch (ch) {
        case 'T':
            return Trit::T;
            break;
        case 'O':
            return Trit::O;
            break;
        case 'I':
            return Trit::I;
            break;
        }
    }

    static const char *add(int first, int second)
    {
        return additionTable[first+1][second+1];
    }

    static int mult(int first, int second)
    {
        return getStateFromChar(multiplicationTable[first+1][second+1]);
    }
};

#endif // TRIT_H
