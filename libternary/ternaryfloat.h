#ifndef TernaryFloat_H
#define TernaryFloat_H

#include "trit.h"
#include "ternaryint.h"

class TernaryFloat
{
    Trit *value;
    const int size = 27;
public:
    TernaryFloat();

    void fromDecimal(float number);
    float toDecimal();

    TernaryFloat& fromString(const std::string &str);
    std::string toString();
};

#endif // TernaryFloat_H
