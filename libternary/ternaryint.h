
#ifndef TernaryInt_H
#define TernaryInt_H

#include "trit.h"
#include "tryte.h"
#include <string>
#include <math.h>

class TernaryInt
{
    Trit *value;
    const int size = 27;

public:
    TernaryInt();
    TernaryInt(const TernaryInt &tr);
    ~TernaryInt();

    const TernaryInt& operator = (const TernaryInt &tr);
    const TernaryInt& operator = (const int &num);

    TernaryInt operator +(const TernaryInt& tr);
    TernaryInt operator -(const TernaryInt& tr);

    TernaryInt operator *(const TernaryInt& tr);
    TernaryInt operator /(TernaryInt& tr);

    int operator <(const TernaryInt& tr);
    int operator >(const TernaryInt& tr);
    int operator ==(const TernaryInt& tr);
    int operator !=(const TernaryInt& rt);

    TernaryInt& inverse();
    int capacity();
    int sign();
    TernaryInt abs();

    int isNull();
    int sizeOf();

    void insertBackTrit(int state);

    void fromDecimal(int number);
    int toDecimal();

    void fromTryte(Tryte *tryte);

    TernaryInt& fromString(const std::string &str);
    std::string toString();
};

#endif // TernaryInt_H
