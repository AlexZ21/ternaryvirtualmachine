#include "tryte.h"

Tryte::Tryte()
{
    value = new Trit[size];
    for(int i = 0; i < size; i++)
        value[i].state = Trit::O;
}

Tryte::~Tryte()
{
    delete value;
}

const Tryte &Tryte::operator =(const Tryte &tr)
{
    for(int i = 0; i < size; i++)
        value[i].state = tr.value[i].state;

    return *this;
}

const Tryte &Tryte::operator =(const int &num)
{
    fromDecimal(num);
    return *this;
}

int Tryte::operator <(const Tryte &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state < tr.value[i].state)
            return Trit::I;
        else if(value[i].state > tr.value[i].state)
            return Trit::T;
    return Trit::O;
}

int Tryte::operator >(const Tryte &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state > tr.value[i].state)
            return Trit::I;
        else if(value[i].state < tr.value[i].state)
            return Trit::T;
    return Trit::O;
}

int Tryte::operator ==(const Tryte &tr)
{
    bool equally = true;
    for(int i = 0; i < size; i++)
        if(value[i].state != tr.value[i].state )
            equally = false;

    if(equally)
        return Trit::I;
    else
        return Trit::T;
}

int Tryte::operator !=(const Tryte &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state != tr.value[i].state)
            return Trit::I;
    return Trit::T;
}

int Tryte::sizeOf()
{
    return size;
}

Trit *Tryte::getTritArray()
{
    return value;
}

void Tryte::insertBackTrit(int state)
{
    for(int i = 0; i < size; i++)
    {
        if(i != size-1)
            value[i].state = value[i+1].state;
        else
            value[i].state = state;
    }
}

Tryte &Tryte::inverse()
{
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            value[i].state = Trit::O;
            break;
        case Trit::T:
            value[i].state = Trit::I;
            break;
        case Trit::I:
            value[i].state = Trit::T;
            break;
        default:
            break;
        }
    }
    return *this;
}

Tryte &Tryte::fromDecimal(int number)
{
    for(int i = 0; i < size; i++)
        value[i].state = Trit::O;

    int newNumber = number;
    if(newNumber < 0)
        newNumber = -newNumber;

    for(int i = size-1; i >= 0; i--)
    {
        int ceil = newNumber/3;
        int res = newNumber % 3;

        switch (res) {
        case 2:
            value[i].state = Trit::T;
            ceil++;
            newNumber = ceil;
            break;
        case 1:
            value[i].state = Trit::I;
            newNumber = ceil;
            break;
        case 0:
            value[i].state = Trit::O;
            newNumber = ceil;
            break;
        default:
            break;
        }

        if(ceil == 0)
            break;
    }

    if(number < 0)
        inverse();

    return *this;
}

int Tryte::toDecimal()
{
    int out = 0;
    for(int i = 0; i < size; i++)
        out += pow(3,(size-i-1))*value[i].state;

    return out;
}

Tryte &Tryte::fromString(const std::string &str)
{
    if(str.size() == size)
    {
        for(int i = 0; i < size; i++)
            value[i].state = Trit::getStateFromChar(str[i]);
    }else if(str.size() < size){
        for(int i = 0; i < size; i++)
            value[i].state = Trit::O;
        for(int i = 0; i < str.size(); i++)
        {
            value[(size - str.size())+i].state = Trit::getStateFromChar(str[i]);
        }
    }
    return *this;
}

std::string Tryte::toString()
{
    std::string str;
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            str.push_back('O');
            break;
        case Trit::T:
            str.push_back('T');
            break;
        case Trit::I:
            str.push_back('I');
            break;
        default:
            break;
        }
    }
    return str;
}
