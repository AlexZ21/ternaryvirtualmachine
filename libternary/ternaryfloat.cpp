#include "ternaryfloat.h"
#include <cmath>

TernaryFloat::TernaryFloat()
{
    value = new Trit[size];
    for(int i = 0; i < size; i++)
    {
        if(i < 8) value[i].state = Trit::T;
        else value[i].state = Trit::O;
    }
}

void TernaryFloat::fromDecimal(float number)
{

}

float TernaryFloat::toDecimal()
{

}

TernaryFloat &TernaryFloat::fromString(const std::string &str)
{
    if(str.size() == size)
    {
        for(int i = 0; i < size; i++)
            value[i].state = Trit::getStateFromChar(str[i]);
    }else if(str.size() < size){
        for(int i = 0; i < size; i++)
            value[i].state = Trit::O;
        for(int i = 0; i < str.size(); i++)
        {
            value[(size - str.size())+i].state = Trit::getStateFromChar(str[i]);
        }
    }
    return *this;
}

std::string TernaryFloat::toString()
{
    std::string str;
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            str.push_back('O');
            break;
        case Trit::T:
            str.push_back('T');
            break;
        case Trit::I:
            str.push_back('I');
            break;
        default:
            break;
        }
    }
    return str;
}
