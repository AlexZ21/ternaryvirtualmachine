#include "ternaryint.h"

TernaryInt::TernaryInt()
{
    value = new Trit[size];
    for(int i = 0; i < size; i++)
        value[i].state = Trit::O;
}

TernaryInt::TernaryInt(const TernaryInt &tr)
{
    value = new Trit[size];
    for(int i = 0; i < size; i++)
        value[i].state = tr.value[i].state;
}

TernaryInt::~TernaryInt()
{
    delete value;
}

const TernaryInt &TernaryInt::operator =(const TernaryInt &tr)
{
    for(int i = 0; i < size; i++)
        value[i].state = tr.value[i].state;

    return *this;
}

const TernaryInt &TernaryInt::operator =(const int &num)
{
    fromDecimal(num);
    return *this;
}

TernaryInt TernaryInt::operator +(const TernaryInt &tr)
{
    TernaryInt result;
    TernaryInt tmp;

    for(int i = size-1; i >= 0; i--)
    {
        const char *sum = Trit::add(value[i].state, tr.value[i].state);
        result.value[i].state = Trit::getStateFromChar(sum[1]);
        if(i > 0) tmp.value[i-1].state = Trit::getStateFromChar(sum[0]);
    }

    if(tmp.isNull() == Trit::T)
        result = result + tmp;

    return result;
}

TernaryInt TernaryInt::operator -(const TernaryInt &tr)
{
    TernaryInt res = tr;
    return *this + res.inverse();
}

TernaryInt TernaryInt::operator *(const TernaryInt &tr)
{
    TernaryInt result;

    for(int n = 0; n < size; n++)
    {
        TernaryInt tmp;
        for(int i = size-1; i >= 0; i--)
        {
            if(i-n >= 0) tmp.value[i-n].state = Trit::mult(value[i].state, tr.value[size-n-1].state);
        }

        if(tmp.isNull() == Trit::T)
            result = result + tmp;
    }

    return result;
}

TernaryInt TernaryInt::operator /(TernaryInt &tr)
{
    TernaryInt divider = tr;
    TernaryInt doubleDivider = tr + tr;
    TernaryInt doubleDividend = (*this) + (*this);

    TernaryInt tmp;
    TernaryInt result;

    if(divider.sign() < 0)
        divider.inverse();

    if((divider > TernaryInt()) == Trit::I)
    {
        for(int i = 0; i < size; i++ )
        {
            tmp.insertBackTrit(doubleDividend.value[i].state);

            if((tmp.abs() > divider.abs()) == Trit::I || ((tmp.abs() > divider.abs()) == Trit::O))
            {
                if(tmp.sign() == divider.sign())
                {
                    tmp = tmp - doubleDivider;
                    result.insertBackTrit(Trit::I);
                }else{
                    tmp = tmp + doubleDivider;
                    result.insertBackTrit(Trit::T);
                }

            }else{
                result.insertBackTrit(Trit::O);
            }
        }

    }

    return result;
}

int TernaryInt::operator <(const TernaryInt &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state < tr.value[i].state)
            return Trit::I;
        else if(value[i].state > tr.value[i].state)
            return Trit::T;
    return Trit::O;
}

int TernaryInt::operator >(const TernaryInt &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state > tr.value[i].state)
            return Trit::I;
        else if(value[i].state < tr.value[i].state)
            return Trit::T;
    return Trit::O;
}

int TernaryInt::operator ==(const TernaryInt &tr)
{
    bool equally = true;
    for(int i = 0; i < size; i++)
        if(value[i].state != tr.value[i].state )
            equally = false;

    if(equally)
        return Trit::I;
    else
        return Trit::T;
}

int TernaryInt::operator !=(const TernaryInt &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state != tr.value[i].state)
            return Trit::I;
    return Trit::T;
}

TernaryInt &TernaryInt::inverse()
{
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            value[i].state = Trit::O;
            break;
        case Trit::T:
            value[i].state = Trit::I;
            break;
        case Trit::I:
            value[i].state = Trit::T;
            break;
        default:
            break;
        }
    }
    return *this;
}

int TernaryInt::capacity()
{
    for(int i = 0; i < size; i++)
    {
        if(value[i].state != Trit::O)
            return size - i;
    }

    return 0;
}

int TernaryInt::sign()
{
    for(int i = 0; i < size; i++)
        if(value[i].state != Trit::O)
            return value[i].state;
}

TernaryInt TernaryInt::abs()
{
    TernaryInt tmp = *this;
    if((tmp < TernaryInt()) == Trit::I)
    {
        return tmp.inverse();
    }else{
        return tmp;
    }
}

int TernaryInt::isNull()
{
    bool null = true;
    for(int i = 0; i < size; i++)
        if(value[i].state != Trit::O)
            null = false;

    if(null)
        return Trit::I;
    else
        return Trit::T;
}

void TernaryInt::fromDecimal(int number)
{
    for(int i = 0; i < size; i++)
        value[i].state = Trit::O;

    int newNumber = number;
    if(newNumber < 0)
        newNumber = -newNumber;

    for(int i = size-1; i >= 0; i--)
    {
        int ceil = newNumber/3;
        int res = newNumber % 3;

        switch (res) {
        case 2:
            value[i].state = Trit::T;
            ceil++;
            newNumber = ceil;
            break;
        case 1:
            value[i].state = Trit::I;
            newNumber = ceil;
            break;
        case 0:
            value[i].state = Trit::O;
            newNumber = ceil;
            break;
        default:
            break;
        }

        if(ceil == 0)
            break;
    }

    if(number < 0)
        inverse();
}

int TernaryInt::toDecimal()
{
    int out = 0;
    for(int i = 0; i < size; i++)
        out += pow(3,(size-i-1))*value[i].state;

    return out;
}

void TernaryInt::fromTryte(Tryte *tryte)
{
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < tryte[i].sizeOf(); j++)
            insertBackTrit(tryte[i].getTritArray()[j].state);
//    for(int j = 0; j < tryte[2].sizeOf(); j++)
//             insertBackTrit(tryte[2].getTritArray()[j].state);
}

int TernaryInt::sizeOf()
{
    return size;
}

void TernaryInt::insertBackTrit(int state)
{
    for(int i = 0; i < size; i++)
    {
        if(i != size-1)
            value[i].state = value[i+1].state;
        else
            value[i].state = state;
    }
}

TernaryInt &TernaryInt::fromString(const std::string &str)
{
    if(str.size() == size)
    {
        for(int i = 0; i < size; i++)
            value[i].state = Trit::getStateFromChar(str[i]);
    }else if(str.size() < size){
        for(int i = 0; i < size; i++)
            value[i].state = Trit::O;
        for(int i = 0; i < str.size(); i++)
        {
            value[(size - str.size())+i].state = Trit::getStateFromChar(str[i]);
        }
    }
    return *this;
}

std::string TernaryInt::toString()
{
    std::string str;
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            str.push_back('O');
            break;
        case Trit::T:
            str.push_back('T');
            break;
        case Trit::I:
            str.push_back('I');
            break;
        default:
            break;
        }
    }
    return str;
}
