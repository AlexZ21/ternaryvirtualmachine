#include "trit.h"

const char *Trit::additionTable[3][3] = { "TI", "OT", "OO",
                                          "OT", "OO", "OI",
                                          "OO", "OI", "IT"};
const char Trit::multiplicationTable[3][3] = { 'I', 'O', 'T',
                                               'O', 'O', 'O',
                                               'T', 'O', 'I'};
