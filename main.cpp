#include "log/log.h"

#include "tvm.h"

int main()
{
    Log::startLog("log.txt", true);

    TVM::Core *core = new TVM::Core(81);

    TVM::Monitor *monitor = new TVM::Monitor(core);

    core->start();
    monitor->start();
    monitor->wait();

    Log::closeLog();


    delete core;
    delete monitor;

    return 0;
}

