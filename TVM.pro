TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -pthread

SOURCES += main.cpp \
    libternary/ternaryfloat.cpp \
    libternary/ternaryint.cpp \
    libternary/trit.cpp \
    libternary/tryte.cpp \
    tvm/core.cpp \
    tvm/memory.cpp \
    log/log.cpp \
    tvm/cmd.cpp \
    tvm/monitor.cpp \
    tvm/registers.cpp

HEADERS += \
    libternary/ternaryfloat.h \
    libternary/ternaryint.h \
    libternary/ternarylib.h \
    libternary/trit.h \
    libternary/tryte.h \
    tvm/core.h \
    tvm.h \
    tvm/registers.h \
    tvm/stack.h \
    tvm/memory.h \
    log/log.h \
    tvm/word.h \
    tvm/cmd.h \
    tvm/thread.h \
    tvm/monitor.h

