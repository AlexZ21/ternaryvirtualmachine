#ifndef CORE_H
#define CORE_H


#include "thread.h"
#include "memory.h"
#include "registers.h"
#include "cmd.h"

namespace TVM{


class Core: public TVM::Thread
{
//    std::vector<TernaryInt> *errorVector;
    Memory *memory;
    Registers *registers;
    Cmd *cmd;

    bool state;

public:
    Core(int memorySize);
    ~Core();
    void run();
    void stop();

    Registers *getRegisters();
};



}

#endif // CORE_H
