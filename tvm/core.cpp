#include "core.h"

TVM::Core::Core(int memorySize)
{

    this->registers = new Registers();
    this->memory = new Memory(memorySize, this->registers);
    Tryte memSize;
    memSize.fromDecimal(memorySize);
    registers->getRegister("MEMCAP")->setValue(memSize);
    this->cmd = new Cmd(memory, registers);

    this->state = true;

    memory->getTryte(Tryte().fromDecimal(0))->getTritArray()[8].state = -1;
    memory->getTryte(Tryte().fromDecimal(0))->getTritArray()[7].state = -1;
    memory->getTryte(Tryte().fromDecimal(0))->getTritArray()[6].state = -1;
    Log::print()<<memory->getTryte(Tryte().fromDecimal(0))->toString();
    cmd->process(Tryte().fromDecimal(0));
}

TVM::Core::~Core()
{
    if(this->memory) delete this->memory;
    delete this->registers;
    delete this->cmd;
}

void TVM::Core::run()
{
    while(this->state)
    {

    }
}

void TVM::Core::stop()
{
    this->state = false;
}

TVM::Registers *TVM::Core::getRegisters()
{
    return this->registers;
}
