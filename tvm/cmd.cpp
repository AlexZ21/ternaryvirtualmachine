#include "cmd.h"


TVM::Cmd::Cmd(Memory *mem, Registers *reg)
{
    this->memory = mem;
    this->registers = reg;
}

void TVM::Cmd::setMemory(Memory *value)
{
    memory = value;
}

void TVM::Cmd::setRegisters(Registers *value)
{
    registers = value;
}

void TVM::Cmd::process(Tryte &address)
{
    Word w;
    w.setTryte(memory->getTryte(address));

    Tryte cmd;
    for(int i = 2; i<=6; i++)
        cmd.insertBackTrit(w.getTrit(i)->state);

    Tryte reg;
    for(int i = 7; i<=9; i++)
        reg.insertBackTrit(w.getTrit(i)->state);
    Log::print()<<reg.toString();
}



