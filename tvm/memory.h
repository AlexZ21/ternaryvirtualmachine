#ifndef MEMORY_H
#define MEMORY_H

#include "libternary/ternarylib.h"
#include "stack.h"
#include "registers.h"


#include "log/log.h"


namespace TVM{

class Memory
{
    int size;
    Tryte *memory;
    Stack *dataStack;
    Stack *callStack;
    Registers *registers;
public:
    Memory(int size, Registers *registers);
    ~Memory();

    Tryte *getTryte(Tryte &cell);
    Tryte *writeTryte(Tryte &cell, Tryte &value);

    void *pushToDataStack(Word *value);
    void *popFromDataStack();

    Word *pushToCallStack(Word *value);
    Word *popFromCallStack();
};

}

#endif // MEMORY_H
