#ifndef WORD_H
#define WORD_H

#include "libternary/ternarylib.h"
#include "log/log.h"

namespace TVM{

class Word
{
    const int size = 3;
    const int tritSize = 27;
    Tryte *cellPointer = NULL;
    bool pointerFlag;
public:
    Word()
    {
        pointerFlag = false;
    }

    void setValue(Tryte &value)
    {
        pointerFlag = false;
        Tryte *val = new Tryte[3];
        val[2] = value;
        this->cellPointer = val;
    }

    void setTryte(Tryte *cellPointer)
    {
        pointerFlag = true;
        this->cellPointer = cellPointer;
    }

    Tryte *getTryte()
    {
        return this->cellPointer;
    }

    Trit *getTrit(int i)
    {
        if(i <= tritSize && i > 0)
        {
            int cellNumber = i/cellPointer->sizeOf();
            int offset = i % cellPointer->sizeOf();
            if(offset == 0)
            {
                cellNumber--;
                offset = cellPointer->sizeOf();
            }
            return &cellPointer[cellNumber].getTritArray()[offset-1];
        }else{
            return NULL;
        }
    }

    const Word& operator = (const Word &tr)
    {
        this->cellPointer  = tr.cellPointer;
        return *this;
    }

    TernaryInt toTernaryInt(){
        TernaryInt tmp;
        for(int i = 1; i <= tritSize; i++)
            tmp.insertBackTrit(getTrit(i)->state);
        return tmp;
    }


};

}

#endif // WORD_H
