#include "monitor.h"


TVM::Monitor::Monitor(Core *core = NULL)
{
    if(core) this->core = core;
    this->state = true;
}

void TVM::Monitor::setMonitor(TVM::Core *core = NULL)
{
    if(core) this->core = core;
}

void TVM::Monitor::run()
{
    Log::print()<<"TVM staring...";
    while(this->state)
    {
        std::string line;
        std::getline(std::cin, line);

        if(line == "EXIT")
        {
            this->state = false;
        }

        if(line == "MEMCAP")
        {
            Log::print()<<this->core->getRegisters()->getRegister(line)->toTernaryInt().toDecimal();
        }
    }
    Log::print()<<"TVM stoping...";
}

void TVM::Monitor::stop()
{
    this->state = false;
    if(core) this->core->stop();
}
