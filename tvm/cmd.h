#ifndef CMD_H
#define CMD_H

#include "libternary/ternarylib.h"
#include "registers.h"
#include "memory.h"
#include "log/log.h"

namespace TVM{

class Cmd
{
    enum CommandList{
        MOVE = 0
    };


    Memory *memory;
    Registers *registers;

public:
    Cmd(Memory *mem, Registers *reg);
    void setMemory(Memory *value);
    void setRegisters(Registers *value);

    void process(Tryte &address);
};

}

#endif // CMD_H
