#include "registers.h"


TVM::Registers::Registers()
{
    regNameMap.insert(std::pair<std::string, int>("MEMCAP", 0));
    regMap.insert(std::pair<int, Word *>(0, new Word()));


    regNameMap.insert(std::pair<std::string, int>("CSP", 1));
    regMap.insert(std::pair<int, Word *>(1, NULL));

    regNameMap.insert(std::pair<std::string, int>("CSPL", 2));
    regMap.insert(std::pair<int, Word *>(1, NULL));

    regNameMap.insert(std::pair<std::string, int>("CSPH", 3));
    regMap.insert(std::pair<int, Word *>(1, NULL));


    regNameMap.insert(std::pair<std::string, int>("DSP", 4));
    regMap.insert(std::pair<int, Word *>(4, NULL));

    regNameMap.insert(std::pair<std::string, int>("DSPL", 5));
    regMap.insert(std::pair<int, Word *>(5, NULL));

    regNameMap.insert(std::pair<std::string, int>("DSPH", 6));
    regMap.insert(std::pair<int, Word *>(6, NULL));


    regNameMap.insert(std::pair<std::string, int>("EXC", 7));
    regMap.insert(std::pair<int, Word *>(7, NULL));


    regNameMap.insert(std::pair<std::string, int>("PC", 8));
    regMap.insert(std::pair<int, Word *>(8, new Word()));


    regNameMap.insert(std::pair<std::string, int>("R0", 9));
    regMap.insert(std::pair<int, Word *>(9, new Word()));

    regNameMap.insert(std::pair<std::string, int>("R1", 10));
    regMap.insert(std::pair<int, Word *>(10, new Word()));

    regNameMap.insert(std::pair<std::string, int>("R2", 11));
    regMap.insert(std::pair<int, Word *>(11, new Word()));

    regNameMap.insert(std::pair<std::string, int>("R3", 12));
    regMap.insert(std::pair<int, Word *>(12, new Word()));


    regNameMap.insert(std::pair<std::string, int>("IVBASE", 13));
    regMap.insert(std::pair<int, Word *>(13, new Word()));


    regNameMap.insert(std::pair<std::string, int>("IC", 14));
    regMap.insert(std::pair<int, Word *>(14, new Word()));

    regNameMap.insert(std::pair<std::string, int>("ICL", 15));
    regMap.insert(std::pair<int, Word *>(15, new Word()));


}

TVM::Word *TVM::Registers::getRegister(std::string name)
{
    if(regNameMap.find(name) != regNameMap.end())
    {
        return regMap.at(regNameMap.at(name));
    }else{
        return NULL;
    }

}

TVM::Word *TVM::Registers::getRegister(Tryte &tr)
{
    if(regMap.find(tr.toDecimal()) != regMap.end())
    {
        return regMap.at(tr.toDecimal());
    }else{
        return NULL;
    }
}

void TVM::Registers::writeRegister(std::string name, TVM::Word *value)
{
    if(regNameMap.find(name) != regNameMap.end())
    {
        regMap[regNameMap.at(name)] = value;
    }
}

void TVM::Registers::writeRegister(Tryte &tr, TVM::Word *value)
{
    if(regMap.find(tr.toDecimal()) != regMap.end())
    {
        regMap[tr.toDecimal()] = value;
    }
}
