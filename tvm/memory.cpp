#include "memory.h"


TVM::Memory::Memory(int size, Registers *registers)
{
    this->registers = registers;
    this->size = size;

    memory = new Tryte[this->size];
    dataStack = new Stack(9, &memory[0]);
    this->registers->writeRegister(Tryte().fromDecimal(2),dataStack->getFirst());
    this->registers->writeRegister(Tryte().fromDecimal(3),dataStack->getLast());

    callStack = new Stack(9, &memory[27]);
    this->registers->writeRegister(Tryte().fromDecimal(5),callStack->getFirst());
    this->registers->writeRegister(Tryte().fromDecimal(6),callStack->getLast());

}

TVM::Memory::~Memory()
{

    delete []memory;
}

Tryte *TVM::Memory::getTryte(Tryte &cell)
{
    int c = (this->size/2) + cell.toDecimal();
    if(c >= 0 && c < size)
        return &this->memory[c];
    else
        NULL;
}

Tryte *TVM::Memory::writeTryte(Tryte &cell, Tryte &value)
{
    Tryte *tr = NULL;
    tr = this->getTryte(cell);
    if(tr)
    {
        *tr = value;
        return tr;
    }else{
        return NULL;
    }
}

void *TVM::Memory::pushToDataStack(TVM::Word *value)
{
    registers->writeRegister(Tryte().fromDecimal(2),dataStack->push(value));
}

void *TVM::Memory::popFromDataStack()
{
    registers->writeRegister(Tryte().fromDecimal(3),dataStack->pop());
}

TVM::Word *TVM::Memory::pushToCallStack(TVM::Word *value)
{
    registers->writeRegister(Tryte().fromDecimal(5),callStack->push(value));
}

TVM::Word *TVM::Memory::popFromCallStack()
{
    registers->writeRegister(Tryte().fromDecimal(6),callStack->pop());
}
