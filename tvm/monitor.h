#ifndef MONITOR_H
#define MONITOR_H

#include <iostream>
#include <string>
#include "core.h"
#include "thread.h"

#include "log/log.h"

namespace TVM{


class Monitor: public TVM::Thread
{
    Core *core;
    bool state;
public:
    Monitor(Core *core);

    void setMonitor(Core *core);

    void run();
    void stop();
};

}

#endif // MONITOR_H
