#ifndef STACK_H
#define STACK_H

#include "libternary/ternarylib.h"
#include "word.h"
#include "registers.h"

namespace TVM{

class Stack
{
    int size;
    int maxSize;
    Tryte *firstTryte;
    Word *first;
    Word *last;
public:
    Stack(int size, Tryte *firstTryte){
        this->maxSize = size;
        this->firstTryte = firstTryte;
        this->first = new Word();
        this->first->setTryte(firstTryte);
        this->last = first;
    }

    ~Stack(){
        delete this->first;
    }

    Word *getFirst(){
        return this->first;
    }

    Word *getLast(){
        return this->last;
    }

    Word *push(Word *value){

        if(size < maxSize)
        {
            this->size++;
            this->first[size] = *value;
            this->last = &this->first[size];
        }

        return this->last;
    }

    Word *pop(){
        if(size > 0)
        {
            this->size--;
            this->last = &this->first[size];
        }
        return this->last;
    }
};

}

#endif // STACK_H
