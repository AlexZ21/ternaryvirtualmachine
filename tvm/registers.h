#ifndef REGISTERS_H
#define REGISTERS_H

#include "libternary/ternarylib.h"
#include "word.h"
#include <string>
#include <map>


namespace TVM{

class Registers
{
    std::map<std::string, int> regNameMap;
    std::map<int, Word *> regMap;
public:
    Registers();
    Word MEMCAP;

    Word *getRegister(std::string name);
    Word *getRegister(Tryte &tr);

    void writeRegister(std::string name, Word *value);
    void writeRegister(Tryte &tr, Word *value);
};

}

#endif // REGISTERS_H
